// Modules
const express = require("express");
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const env = require('dotenv').config();
const mongo = require('./mongo.js');
const mongoose = require('mongoose');
const crypto = require('crypto');

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';
const JWT_SECRET_KEY=process.env.JWT_SECRET_KEY
const Consumer = mongoose.model('consumer', new mongoose.Schema({email: String}), "consumers");
const Deliverer = mongoose.model('deliverer', new mongoose.Schema({email: String}), "deliverers");
const Restaurant = mongoose.model('restaurant', new mongoose.Schema({email: String}), "restaurants");

let Model = null;

// APP
const app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get("/", function(req, res) {
  console.log("GET")

  res.json({"success": "Hello from auth-server"});
});

app.post('/login', function(request, response) {

  console.log("POST LOGIN")

  const email = request.body.email;
  const password = request.body.password;
  const platform = request.body.platform;

  console.log(email)
  console.log(password)
  console.log(platform)

  if(!platform) {
    response.status(400).send('plateform not found');
    return;
  } else if(platform == "consumer") {
    Model = Consumer;
  } else if(platform == "restaurant") {
    Model = Restaurant;
  } else if(platform == "deliverer") {
    Model = Deliverer;
  }

  const foundUser = findUserInDbByEmail(email);
  if (!foundUser) {
    response.status(400).send('user not found');
    return;
  }

  const isPasswordCorrect = checkPassword(foundUser, password);
  if (!isPasswordCorrect) {
    response.status(400).send('incorrect password');
    return;
  }

  const accessToken = genAccessToken(foundUser);            // new
  const refreshToken = genRefreshToken(foundUser);          // new
  response.status(200).json({ accessToken, refreshToken }); // new

});

app.post('/refreshToken', function(request, response) {
  console.log("POST REFRESH TOKEN")

  const refreshToken = request.body.refreshToken;

  try {
    const tokenPayload = jwt.verify(refreshToken, JWT_SECRET_KEY);
    if (tokenPayload.type !== 'refresh') {
        throw new Error('wrong token type');
    }
    const userId = tokenPayload.userId;
    const userInDb = findUserById(userId);
    const password = userInDb.password;
    const keyToCompare = genKey(userId, password);
    if (keyToCompare !== tokenPayload.key) {
      throw new Error('password changed');
    }

    const newAccessToken = genAccessToken(userInDb);
    response.status(200).json({ accessToken });
  } catch (error) {
    response.status(401).send(error.message);
  }
});

app.get('/sensitiveInfo', authenticationMiddleware, function (request, response) {
  const userInfo = response.locals.user;
  const userId = userInfo.userId
  if (userInfo.role === 'admin') {
    const sensitiveInfo = getSensitiveInfoFromDb(userId);
    response.status(200).json(sensitiveInfo);
    return;
  }

  response.status(403).send('forbidden request');
});


function authenticationMiddleware(request, response, nextHandler) {
  const accessToken = getAccessTokenFromHeader(request);

  try {
    const tokenPayload = jwt.verify(accessToken, JWT_SECRET_KEY);
    if (tokenPayload.type !== 'access') {      // new
        throw new Error('wrong token type');    // new
    }                                          // new
    response.locals.user = tokenPayload;
    nextHandler();
  } catch (error) {
    response.status(401).send(error.message);
  }
}

function genRefreshToken(user) {
  const userId = user._id;
  const role = user.role;
  const type = 'refresh';
  const password = user.password;
  const key = genKey(userId, password);

  const tokenPayload = { type, userId, role, key };

  const refreshToken = jwt.sign(tokenPayload, JWT_SECRET_KEY);
  return refreshToken;
}

const FIFTEEN_MINUTES_IN_SECOND = 15 * 60;

function genAccessToken(user) {
  const userId = user._id;
  const role = user.role;
  const type = 'access';                         // new

  const tokenPayload = { type, userId, role };   // new

  const accessToken = jwt.sign(
    tokenPayload,
    JWT_SECRET_KEY,
    { expiresIn: FIFTEEN_MINUTES_IN_SECOND }     // new
  );
  return accessToken;
}

function findUserInDbByEmail(email) {
  const filter = {
    email: email
  };
  return Model.find(filter);
}

function checkPassword(email, password) {
  const filter = {
    email: email,
    password: password
  };
  return Model.find(filter);
}

function genKey(id, password) {
  const rawKey = id + password;
  const key = hashHmacSha256(rawKey, JWT_SECRET_KEY);
  return key;
}

function getAccessTokenFromHeader(request) {
  return request.header.Authorization;
}

function getSensitiveInfoFromDb(userId) {
  return {"sensitiveInfo" : "This is a sensitiveInfo accessible only for admins :)"}
}

function hashHmacSha256(s) {
  return crypto
    .createHmac('sha256', JWT_SECRET_KEY)
    .update(s)
    .digest('hex');
}


app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
module.exports = app;